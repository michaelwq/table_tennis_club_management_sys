package com.mercury.ApiGateway.security;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.mercury.ApiGateway.security.handler.AccessDeniedHandlerImpl;
import com.mercury.ApiGateway.security.handler.AuthenticationEntryPointImpl;
import com.mercury.ApiGateway.security.handler.AuthenticationFailureHandlerImpl;
import com.mercury.ApiGateway.security.handler.AuthenticationSuccessHandlerImpl;
import com.mercury.ApiGateway.security.handler.LogoutSuccessHandlerImpl;

@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Autowired
	private AuthenticationEntryPointImpl authenticationEntryPointImpl;
	
	@Autowired
	private AccessDeniedHandlerImpl accessDeniedHandlerImpl;
	
	@Autowired
	private AuthenticationSuccessHandlerImpl authenticationSuccessHandlerImpl;
	
	@Autowired
	private AuthenticationFailureHandlerImpl authenticationFailureHandlerImpl;
	
	@Autowired
	private LogoutSuccessHandlerImpl logoutSuccessHandlerImpl;


	@Override
	public void configure(HttpSecurity http) throws Exception {
		
		http
			.cors()
				.and()
			.csrf()
				.disable()
			.authorizeRequests()
				.antMatchers("/login").permitAll()
//				.anyRequest().authenticated()
				.and()
			.exceptionHandling()
				.accessDeniedHandler(accessDeniedHandlerImpl)
				.authenticationEntryPoint(authenticationEntryPointImpl)
				.and()
			.formLogin()
				.permitAll()
				.loginPage("/login")
				.usernameParameter("username")
				.passwordParameter("password")
				.successHandler(authenticationSuccessHandlerImpl)
				.failureHandler(authenticationFailureHandlerImpl)
				.and()
			.logout()
				.permitAll()
				.logoutUrl("/logout")
				.logoutSuccessHandler(logoutSuccessHandlerImpl)
				.and()
			.httpBasic();

		
	}
	
	 @Bean
	 public CorsConfigurationSource corsConfigurationSource() {
		 CorsConfiguration configuration = new CorsConfiguration();
	     configuration.addAllowedOrigin("*"); // You should only set trusted site here. e.g. http://localhost:4200 means only this site can access.
	     configuration.setAllowedMethods(Arrays.asList("GET","POST","PUT","DELETE","HEAD","OPTIONS"));
	     configuration.addAllowedHeader("*");
	     configuration.setAllowCredentials(true);
	     UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
	     source.registerCorsConfiguration("/**", configuration);
	     return source;
	 }
	 
	 @Bean		// put the return object into spring container, as a bean
		public PasswordEncoder passwordEncoder() {
			return new BCryptPasswordEncoder(11);
		}

	
}
