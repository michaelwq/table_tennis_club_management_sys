package com.mercury.ApiGateway.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mercury.ApiGateway.beans.User;

public interface UserDao extends JpaRepository<User, Integer> {
	
	User findByUsername(String name);
	
}
