package com.mercury.TtmEvent.beans;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="EVENT_SCHEDULES")
public class EventSchedule {
	
	@Id
	@SequenceGenerator(name="event_schedules_seq_gen", sequenceName="EVENT_SCHEDULES_SEQ", allocationSize = 1)
	@GeneratedValue(generator="event_schedules_seq_gen", strategy=GenerationType.AUTO)
	private int eventId;

	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.DETACH)
	@JoinColumn(name="LOCATION_ID")
	private Location location;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.DETACH)
	@JoinColumn(name="EVENT_TYPE_ID")
	private EventType eventType;
	
	@Column(name="TIME_START")
	private String timeStart;
	
	@Column(name="TIME_END")
	private String timeEnd;
	
	@Column(name="E_DATE")
	private String date;

	public EventSchedule() {
		super();
	}

	public EventSchedule(int eventId, Location location, EventType eventType, String timeStart, String timeEnd,
			String date) {
		super();
		this.eventId = eventId;
		this.location = location;
		this.eventType = eventType;
		this.timeStart = timeStart;
		this.timeEnd = timeEnd;
		this.date = date;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public EventType getEventType() {
		return eventType;
	}

	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}

	public String getTimeStart() {
		return timeStart;
	}

	public void setTimeStart(String timeStart) {
		this.timeStart = timeStart;
	}

	public String getTimeEnd() {
		return timeEnd;
	}

	public void setTimeEnd(String timeEnd) {
		this.timeEnd = timeEnd;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "EventSchedule [eventId=" + eventId + ", location=" + location + ", eventType=" + eventType
				+ ", timeStart=" + timeStart + ", timeEnd=" + timeEnd + ", date=" + date + "]";
	}

}
