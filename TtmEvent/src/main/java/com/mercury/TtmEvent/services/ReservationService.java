package com.mercury.TtmEvent.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mercury.TtmEvent.beans.Reservation;
import com.mercury.TtmEvent.daos.ReservationDao;
import com.mercury.TtmEvent.http.Response;

@Service
public class ReservationService {
	
	@Autowired
	private ReservationDao reservationDao;
	
	public Reservation getById(int id) {
		Optional<Reservation> reservation = reservationDao.findById(id);
		
		if (reservation.isPresent()) {
			return reservation.get();
		} else {
			return null;
		}
	}
	
	public List<Reservation> getAll() {
//		System.out.println("--------->inside reservation dao");
		return reservationDao.findAll();
	}
	
	public Response makeReservation(Reservation reservation) {
		try {
//			System.out.println("--------->inside reservation dao");
			reservationDao.save(reservation);
			return new Response(true);
		} catch (Exception e) {
			return new Response(false, e.getMessage());
		}
	}
	
	public Response editReservation(Reservation reservation) {
		try {
			reservationDao.save(reservation);
			return new Response(true);
		} catch (Exception e) {
			return new Response(false, e.getMessage());
		}
	}
	
	public Response deleteReservationById(int reservationId) {
		try {
			reservationDao.deleteById(reservationId);
			return new Response(true);
		} catch (Exception e) {
			return new Response(false, e.getMessage());
		}
	}
	
}
