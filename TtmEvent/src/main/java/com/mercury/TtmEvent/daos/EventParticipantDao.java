package com.mercury.TtmEvent.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mercury.TtmEvent.beans.EventParticipant;

public interface EventParticipantDao extends JpaRepository<EventParticipant, Integer>{
	
	@Query("select e.memberId from EventParticipant e where e.eventId = :eventId")
	List<Integer> getMemberIdByEventId(@Param("eventId") int eventId); 
	
}
