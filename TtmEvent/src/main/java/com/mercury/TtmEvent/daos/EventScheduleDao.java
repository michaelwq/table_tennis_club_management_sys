package com.mercury.TtmEvent.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mercury.TtmEvent.beans.EventSchedule;

public interface EventScheduleDao extends JpaRepository<EventSchedule, Integer>{
	
	EventSchedule findByEventId(int eventId);
	
}
