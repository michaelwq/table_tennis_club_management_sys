package com.mercury.TtmEvent.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mercury.TtmEvent.beans.Location;
import com.mercury.TtmEvent.services.LocationService;

@RestController
@RequestMapping("/location")
public class LocationController {
	
	@Autowired
	private LocationService locationService;
	
	@GetMapping
	public List<Location> getAllLocations() {
		return locationService.getLocations();
	}
	
	@GetMapping("/{id}")
	public Location getLocationById(@PathVariable int id) {
		return locationService.getById(id);
	}
	
}
