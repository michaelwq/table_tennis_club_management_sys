package com.mercury.TtmEvent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class TtmEventApplication {

	public static void main(String[] args) {
		SpringApplication.run(TtmEventApplication.class, args);
	}

}
