import { Component, OnInit } from '@angular/core';
import {Member} from '../shared/models/member.model';
import {Subscription} from 'rxjs';
import {MemberService} from '../shared/services/member.service';
import {MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.scss']
})

export class MemberComponent implements OnInit {

  getMembers: Subscription;

  dataSource: MatTableDataSource<any> = new MatTableDataSource();
  displayedColumns: string[] = ['memberId', 'name', 'phone', 'tier', 'action'];

  constructor(private ms: MemberService) { }

  ngOnInit() {
    this.getMembers = this.ms.getMembers()
      .subscribe( {
        next: (res: Member[]) => {
          this.ms.members = res;
          // this.dataSource = new MatTableDataSource(this.ms.members);
          this.dataSource.data = res;
          // console.log(res);
          this.dataSource.filterPredicate = (data, filter) => {
            const dataStr: string = (data.memberId + data.memberDetail.name + data.memberDetail.phone + data.membershiptier.tierName)
              .toLowerCase();
            return (dataStr.indexOf(filter) !== -1);
          };
        },
        complete: () => {
          console.log('getMembers complete');
        }
      });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
