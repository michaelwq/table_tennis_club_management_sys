import {Component, OnInit} from '@angular/core';
import {Member} from '../../shared/models/member.model';
import {ActivatedRoute, Router} from '@angular/router';
import {MemberService} from '../../shared/services/member.service';
import {switchMap} from 'rxjs/operators';
import {TierService} from '../../shared/services/tier.service';
import {Subscription} from 'rxjs';
import {MembershipTier} from '../../shared/models/membershipTier.model';


@Component({
  selector: 'app-member-detail',
  templateUrl: './member-detail.component.html',
  styleUrls: ['./member-detail.component.scss']
})
export class MemberDetailComponent implements OnInit {

  memberId: number;
  member: Member;

  tier: string;
  getTier: Subscription;

  constructor(
    private ar: ActivatedRoute,
    private ms: MemberService,
    private ts: TierService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.ar.paramMap
      .pipe(switchMap(params => {
        this.memberId = Number(params.get('memberId'));
        return this.ms.getMember(this.memberId);
      }))
      .subscribe((res: Member) => {
        this.member = res;
      });
  }

  get diagnostic() {
    return JSON.stringify(this.member);
  }

  onSubmit() {
    this.getTier = this.ts.getNameById(this.member.membershiptier.tierId)
      .subscribe({
        next: (res: MembershipTier) => {
          this.tier = res.tierName;
          this.member.membershiptier.tierName = this.tier;
        },
        complete: () => {
          console.log('getTier complete');
        }
      });
    this.ms.editMember(this.member).subscribe(res => {
      if (res.success) {
        this.router.navigate(['/member']);
      }
    });
  }

}
