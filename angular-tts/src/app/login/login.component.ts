import { Component, OnInit } from '@angular/core';
import {AuthService} from '../shared/services/auth.service';
import {Router} from '@angular/router';
import {User} from '../shared/models/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  err = false;


  constructor(private as: AuthService, private router: Router) {}

  ngOnInit() {}

  submit(value) {
    console.log(value);
    this.as.login(value as User)
      .subscribe(res => {
        if (res.success) {
          this.as.user = value;
          sessionStorage.setItem("user", "userId");
          this.as.show=true;
          this.router.navigate(['/location']);
        } else {
          this.err = true;
        }
      });
  }

  // logout() {
  //   this.as.logout()
  //     .subscribe(res => {
  //       console.log('logout');
  //     });
  // }

}
