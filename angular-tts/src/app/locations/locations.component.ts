import { Component, OnInit } from '@angular/core';
import {LocationService} from '../shared/services/location.service';
import {Subscription} from 'rxjs';
import {Location} from '../shared/models/location.model';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss']
})
export class LocationsComponent implements OnInit {

  locations: Location[] = [];
  getLocation: Subscription;

  constructor(private ls: LocationService) {}

  ngOnInit() {
    this.getLocation = this.ls.getLocations()
      .subscribe( {
        next: (res: Location[]) => {
          this.locations = res;
        },
        complete: () => {
          console.log('getLocations complete');
        }
      });
  }

}
