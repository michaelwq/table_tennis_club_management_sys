import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {LocationsComponent} from './locations/locations.component';
import {LocationDetailComponent} from './locations/location-detail/location-detail.component';
import {GearsComponent} from './gears/gears.component';
import {GearDetailComponent} from './gears/gear-detail/gear-detail.component';
import {MemberComponent} from './member/member.component';
import {MemberDetailComponent} from './member/member-detail/member-detail.component';
import {MemberRegistrationComponent} from './member/member-registration/member-registration.component';
import {ReserveComponent} from './reserve/reserve.component';
import {ReservationComponent} from './reservation/reservation.component';
import {ReserveDetailComponent} from './reserve/reserve-detail/reserve-detail.component';
import {EventComponent} from './event/event.component';
import {EventDetailComponent} from './event/event-detail/event-detail.component';
import {EventAddComponent} from './event/event-add/event-add.component';
import {LoginComponent} from './login/login.component';
import {AppGuard} from './app.guard';

const routes: Routes = [
  {
    path: 'home',
    component: LocationsComponent,
    canActivate: [AppGuard]
  },
  {
    path: 'location',
    component: LocationsComponent,
    canActivate: [AppGuard]
  },
  {
    path: 'location-detail/:locationId',
    component: LocationDetailComponent
  },
  {
    path: 'gear-detail/:gearId',
    component: GearDetailComponent
  },
  {
    path: 'gear',
    component: GearsComponent
  },
  {
    path: 'member',
    component: MemberComponent
  },
  {
    path: 'member-detail/:memberId',
    component: MemberDetailComponent
  },
  {
    path: 'member-registration',
    component: MemberRegistrationComponent
  },
  {
    path: 'reservation',
    component: ReservationComponent
  },
  {
    path: 'reservation/:reservationId',
    component: ReserveDetailComponent
  },
  {
    path: 'reserve/:locationId',
    component: ReserveComponent
  },
  {
    path: 'event',
    component: EventComponent
  },
  {
    path: 'event/:eventId',
    component: EventDetailComponent
  },
  {
    path: 'event-add/:locationId',
    component: EventAddComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '**',
    redirectTo: 'home'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
