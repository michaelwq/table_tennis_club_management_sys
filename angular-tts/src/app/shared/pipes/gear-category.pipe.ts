import {Pipe, PipeTransform} from '@angular/core';
import {Gear} from '../models/gear.model';

@Pipe({
  name: 'gearCategory',
  pure: false
})

export class GearCategoryPipe implements PipeTransform {

  transform(gears: Gear[], type: string) {

    if (type === 'All') {
      return gears;
    } else {
      return gears.filter( (g) => {
        return g.category === type;
      });
    }
  }

}
