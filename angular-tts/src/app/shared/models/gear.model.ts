export class Gear {

  public gearId?: number;
  public name: string;
  public brand: string;
  public price: string;
  public category: string;
  public image: string;
  public amount: number;

}
