import { Component, OnInit } from '@angular/core';
import {Member} from '../shared/models/member.model';
import {MemberService} from '../shared/services/member.service';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import {LocationService} from '../shared/services/location.service';
import {Reservation} from '../shared/models/reservation.model';
import {Location} from '../shared/models/location.model';
import {ReservationComponent} from '../reservation/reservation.component';
import {ReservationService} from '../shared/services/reservation.service';

@Component({
  selector: 'app-reserve',
  templateUrl: './reserve.component.html',
  styleUrls: ['./reserve.component.scss']
})
export class ReserveComponent implements OnInit {

  member: Member;
  getMember: Subscription;
  name: string;

  locationId: number;
  location: Location;

  reserve: Reservation = {
    reservationId: 0,
    memberId: 0,
    location: this.location,
    date: '',
    startTime: '',
    endTime: '',
    robot: 0
  };

  clicked: boolean;

  price: number;

  constructor(
    private ms: MemberService,
    private ls: LocationService,
    private rs: ReservationService,
    private ar: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.ar.paramMap
      .pipe(switchMap(params => {
        this.locationId = Number(params.get('locationId'));
        return this.ls.getLocation(this.locationId);
      }))
      .subscribe((res: Location) => {
        this.location = res;
      });
    this.clicked = false;
  }

  findMemberByName() {
    this.getMember = this.ms.getMemberByName(this.name)
      .subscribe( {
        next: (res: Member) => {
          this.member = res;
          console.log(this.member);
        },
        complete: () => {
          console.log('getByName complete');
        }
      });
  }

  addReservation() {
    this.reserve.location = this.location;
    this.reserve.memberId = this.member.memberId;
    // console.log(this.reserve);
    this.clicked = true;
    this.calculatePrice();
    this.rs.addReservation(this.reserve).subscribe();
  }

  calculatePrice() {
    this.price = 0;
    if (this.member.membershiptier.tierId === 1) {
      this.price += 15;
      // tslint:disable-next-line:triple-equals
      if (this.reserve.robot == 1) {
        this.price += 10;
      }
    } else if (this.member.membershiptier.tierId === 2) {
      // tslint:disable-next-line:triple-equals
      if (this.reserve.robot == 1) {
        this.price += 5;
      }
    }
    return this.price;
  }

  back() {
    this.router.navigate(['/location']);
  }

}
