import { Component, OnInit } from '@angular/core';
import {ReservationService} from '../shared/services/reservation.service';
import {Subscription} from 'rxjs';
import {Reservation} from '../shared/models/reservation.model';
import {MatTableDataSource} from '@angular/material';
import {Router} from '@angular/router';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.scss']
})
export class ReservationComponent implements OnInit {

  reservations: Reservation[];
  getReservations: Subscription;

  dataSource: MatTableDataSource<any> = new MatTableDataSource();
  displayedColumns = ['reservationId', 'memberId', 'location', 'date', 'action'];

  constructor(
    private rs: ReservationService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getReservations = this.rs.getReservations()
      .subscribe({
        next: (res: Reservation[]) => {
          // console.log(res);
          this.reservations = res;
          // console.log(this.reservations);
          this.dataSource.data = res;
          this.dataSource.filterPredicate = (data, filter) => {
            const dataStr: string = (data.reservationId + data.memberId + data.location.name + data.date).toLowerCase();
            return (dataStr.indexOf(filter) !== -1);
          };
        }
      });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  delete(id: number) {
    if (confirm('Are you sure to cancel this reservation?')) {
      this.rs.deleteReservationById(id).subscribe( res => {
        if (res.success) {
          this.ngOnInit();
          this.router.navigate(['/reservation']);
        }
      });
    }
  }


}
