import { Component, OnInit } from '@angular/core';
import {AuthService} from '../shared/services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  title = 'SPIN';
  location = 'Location';
  gear = 'Gear';
  member = 'Member';
  reservation = 'Reservation';
  event = 'event';


  constructor(
    private as: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  logout() {
    this.as.logout()
      .subscribe(res => {
        console.log('logout');
        sessionStorage.clear();
        this.router.navigate(['/login']);
      });
  }

}
