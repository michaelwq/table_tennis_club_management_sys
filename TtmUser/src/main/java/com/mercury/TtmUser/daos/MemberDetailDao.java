package com.mercury.TtmUser.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mercury.TtmUser.beans.MemberDetail;

public interface MemberDetailDao extends JpaRepository<MemberDetail, Integer>{
	
	
	
}
