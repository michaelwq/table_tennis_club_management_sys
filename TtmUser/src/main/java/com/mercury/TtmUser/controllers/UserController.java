//package com.mercury.TtmUser.controllers;
//
//import java.util.List;
//
//import org.apache.tomcat.util.net.openssl.ciphers.Authentication;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.mercury.ApiGateway.services.UserService;
//import com.mercury.TtmUser.beans.User;
//import com.mercury.TtmUser.http.Response;
//
//@RestController
//@RequestMapping("/user")
//public class UserController {
//
//	@Autowired
//	private UserService userService;
//	
//	@GetMapping
//	public List<User> getAll() {
//		return userService.getAll();
//	}
//	
//	@PostMapping
//	public Response addUser(@RequestBody User user) {
//		return userService.register(user);
//	}
//	
//	@PutMapping
//	public Response changeUser(@RequestBody User user, Authentication authentication) {
//		return userService.changePassword(user, authentication);
//	}
//	
//	@DeleteMapping("/{id}")
//	public Response deleteUser(@PathVariable int id) {
//		return userService.deleteUser(id);
//	}
//
//}
