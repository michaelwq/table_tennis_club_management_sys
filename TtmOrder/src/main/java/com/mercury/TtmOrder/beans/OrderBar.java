package com.mercury.TtmOrder.beans;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="ORDER_BAR")
public class OrderBar {
	
	@Id
	@SequenceGenerator(name="order_bar_seq_gen", sequenceName="ORDER_BAR_SEQ", allocationSize=1)
	@GeneratedValue(generator="order_bar_seq_gen", strategy=GenerationType.AUTO)
	private int id;
	
	@Column
	private int quantity;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.DETACH)
	@JoinColumn(name="ORDER_ID")
	private OrderFromBar orderFromBar;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.DETACH)
	@JoinColumn(name="PRODUCT_ID")
	private Bar bar;

	public OrderBar() {
		super();
	}

	public OrderBar(int id, int quantity, OrderFromBar orderFromBar, Bar bar) {
		super();
		this.id = id;
		this.quantity = quantity;
		this.orderFromBar = orderFromBar;
		this.bar = bar;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	@JsonIgnore
	public OrderFromBar getOrderFromBar() {
		return orderFromBar;
	}

	public void setOrderFromBar(OrderFromBar orderFromBar) {
		this.orderFromBar = orderFromBar;
	}

	public Bar getBar() {
		return bar;
	}

	public void setBar(Bar bar) {
		this.bar = bar;
	}

	@Override
	public String toString() {
		return "OrderBar [id=" + id + ", quantity=" + quantity + ", orderFromBar=" + orderFromBar + ", bar=" + bar
				+ "]";
	}
	
}
