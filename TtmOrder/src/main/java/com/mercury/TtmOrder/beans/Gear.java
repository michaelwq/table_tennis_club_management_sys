package com.mercury.TtmOrder.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="GEAR")
public class Gear {
	
	@Id
	@SequenceGenerator(name="gear_seq_gen", sequenceName="GEAR_SEQ", allocationSize=1)
	@GeneratedValue(generator="gear_seq_gen", strategy=GenerationType.AUTO)
	private int gearId;
	
	@Column
	private String name;
	
	@Column
	private String brand;
	
	@Column
	private double price;
	
	@Column
	private String category;
	
	@Column
	private String image;
	
	@Column
	private int amount;
	
	public Gear() {
		super();
	}

	public Gear(int gearId, String name, String brand, double price, String category, String image, int amount) {
		super();
		this.gearId = gearId;
		this.name = name;
		this.brand = brand;
		this.price = price;
		this.category = category;
		this.image = image;
		this.amount = amount;
	}

	public int getGearId() {
		return gearId;
	}

	public void setGearId(int gearId) {
		this.gearId = gearId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "Gear [gearId=" + gearId + ", name=" + name + ", brand=" + brand + ", price=" + price + ", category="
				+ category + ", image=" + image + ", amount=" + amount + "]";
	}

}
