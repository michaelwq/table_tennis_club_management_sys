package com.mercury.TtmOrder.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mercury.TtmOrder.beans.OrderFromBar;

public interface OrderFromBarDao extends JpaRepository<OrderFromBar, Integer>{

}
